<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hrtech');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_9EYB>d>ZfcZ>e<_!n}-tll.}d B`1hS:T7`C(qzviSCqO>)f_IPY&Vs.gZWh2Pj');
define('SECURE_AUTH_KEY',  '$utT9juH$dq|(</OAn-P6<gW{a!z5~Md/1}*p@-l<jmjD-!.}/T<+Y`n/{3@5~kj');
define('LOGGED_IN_KEY',    '&k1<Chv#e{$V&_$QMu*jz,71Jn`,~y<q]YzZ|rAWSECq)Et#:SXl=Rpx|4S+wV5[');
define('NONCE_KEY',        'Dvl1fo_kUMtTvVj@,0uP-uhIz}m()aW IGw<2f1%kCzq(qs4utVS<]WAG.KyE-J*');
define('AUTH_SALT',        'C3S vsB-&i.kg1$WE}$?BbO0Dek/QSb#G]kv`_] zE @vD~r)(J /M.$8~f8cgT/');
define('SECURE_AUTH_SALT', 'gw=jXPu7S3(9cfp0E$p!eDOJ/u+HUl1L!tYOhx+0G]F,W/quW2<4Bm?4&fSRU|j(');
define('LOGGED_IN_SALT',   '^x$5qeD*gB);t#RsVf#&E%$nTurXVSziFizUKx.;A_3$F=Ud3hK~lEZwxPI1J$P ');
define('NONCE_SALT',       'H;-t-1h4mnG_mPLIxWbIjYyiB[9fQuR7)V~~T&>vpdYms| _=,:W8yn(K |DZk@=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
