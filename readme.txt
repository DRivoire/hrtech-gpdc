MISE EN PLACE DU SITE
01) S'assurer que les plugins suivant sont installés et activés :
-Advanced custom field PRO ( pour gérer tous les champs personnalisés )
-Contact Form 7 ( pour gérer vos formulaires )
-Timber ( pour la compatibilité du thème )
-Tarteaucitron ( pour ajouter une popin rgpd )
-Yoast SEO ( qui vous servira à optimiser votre référencement )

02) Activé le thème "HR Tech" dans Apparence>Thèmes

03) Créer vos pages d'accueil et d'archive de vos articles dans Pages>Ajouter

04) Définir ces pages comme étant votre page d'accueil et votre page des article dans Réglages>Lecture

05)Les champs personnalisés
Dans ACF>Groupes de champs, vous devriez trouvez plusieurs entrées :
-Articles
-Entreprises
-Front page
-Page d'option

Si ce n'est pas le cas, vous pourrez les importer grâce au fichier acf.json que nous vous avons fourni
vous pourrez l'ajouter dans ACF>Outils


POUR LES DEVS
Le site est géré avec Twig comme moteur de template, timber est le plugin qui permet son utilisation sur le site wordpress
Pour ce qui est des champs personnalisés, la plusparts sont gérés avec ACF, la licence en place est ma license professionnelle
et vous pouvez la laisser en place, je n'ai pour le moment pas de limite de site actif (ce n'est pas garantie que cela reste le cas ...)
