var Filters = {
  init: function () {
    Filters.filtersInit();
  },

  filtersInit: function () {
    window.addEventListener('DOMContentLoaded', function () {
      if (document.querySelector('body').classList.contains('post-type-archive-company')) {
        var reset = document.querySelector('.filter-reset');
        var resetContainer = document.querySelector('.reset-container');
        var noResult = document.querySelector('.card-container__none');
        var grid = document.querySelector('.grid');
        var iso = new Isotope(grid, {
          itemSelector: '.grid-item',
        });
        var filters = document.querySelectorAll('.company-filter__item');

        filters.forEach(function (el) {
          el.addEventListener('click', function () {
            var data = el.dataset.filter;
            resetContainer.classList.add('is-active');

            filters.forEach(function (siblings) {
              if (siblings !== el) {
                siblings.classList.remove('is-active')
              }
            });

            el.classList.add('is-active');

            iso.arrange({
              filter: '.' + data
            });

            if (grid.getElementsByClassName(data).length === 0) {
              noResult.classList.add('active');
            } else {
              noResult.classList.remove('active');
            }

            Filters.scrollToContainer();
          })
        });

        reset.addEventListener('click', function () {
          filters.forEach(function (el) {
            el.classList.remove('is-active')
          });

          iso.arrange({
            filter: '*'
          });

          noResult.classList.remove('active');
          resetContainer.classList.remove('is-active');
          Filters.scrollToContainer();
        })
      }
    });
  },
  scrollToContainer: function () {
    var container = document.getElementById("companies");
    container.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }
};
