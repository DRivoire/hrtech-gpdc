// Declare namespace
var HRTech = HRTech || {};

HRTech.General = function () {
};

HRTech.General.prototype = {

  init: function () {
    //@see layout/header.js
    Header.init();
    Footer.init();
    Players.init();
    Filters.init();

    var rellax = new Rellax('.rellax', {
      breakpoints: [560, 992, 1520]
    });
  }
};

jQuery(document).ready(function () {
  var g = new HRTech.General();
  g.init();
});
