<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});

	add_filter('template_include', function( $template ) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

//custom post type
function wpm_custom_post_type() {
	$labels = array(
		'name'                => _x( 'Entreprises', 'Post Type General Name'),
		'singular_name'       => _x( 'Entreprise', 'Post Type Singular Name'),
		'menu_name'           => __( 'Entreprises'),
		'all_items'           => __( 'Toutes les entreprises'),
		'view_item'           => __( 'Voir l\' entreprises'),
		'add_new_item'        => __( 'Ajouter une entreprise'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer l\' Entreprises'),
		'update_item'         => __( 'Modifier l\' Entreprises'),
		'search_items'        => __( 'Rechercher une entreprise'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);

	$args = array(
		'label'               => __( 'Entreprises'),
		'description'         => __( 'Toute les entreprises'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'company'),

	);

	register_post_type( 'company', $args );

}

add_action( 'init', 'wpm_custom_post_type', 0 );

//taxonimies
add_action( 'init', 'company_create_taxonomy', 0 );
 
function company_create_taxonomy() {
 
  $labels = array(
    'name' => _x( 'secteurs', 'taxonomy general name' ),
    'singular_name' => _x( 'secteurs', 'taxonomy singular name' ),
    'search_items' =>  __( 'Chercher un secteurs' ),
    'all_items' => __( 'Tous les secteurs' ),
    'edit_item' => __( 'Edit secteur' ), 
    'update_item' => __( 'Actualiser secteur' ),
    'add_new_item' => __( 'Ajouter un nouveau secteur' ),
    'new_item_name' => __( 'Nouveau nom de secteur' ),
    'menu_name' => __( 'secteurs' ),
  ); 	
 
  register_taxonomy('sectors',array('company'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'secteur' ),
  ));
}
/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;
/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {

    const PACKAGE_VERSION = '0.0.1';

    /**
     * @var int
     */
    protected $cacheVersion;

	/** Add timber support. */
	public function __construct() {
        $this->cacheVersion = self::PACKAGE_VERSION;
        $this->tplDir = get_template_directory_uri();

		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'add_yoast_breadcrumb' ) );

        if (!is_admin()) {
            add_action('wp_enqueue_scripts', array($this, 'loadStyles'));
            add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        }
		parent::__construct();
	}
	/** This is where you can register custom post types. */



	/** This is where you can register custom taxonomies. */
	// public function register_taxonomies() {

	// }


	/** This is where you can register custom taxonomies. */
	public function add_yoast_breadcrumb() {
		if(function_exists('yoast_breadcrumb')) {
			function breadcrumbs($before = '', $after = '') { return yoast_breadcrumb($before, $after, false); };
			TimberHelper::function_wrapper('breadcrumbs');
		}
	}


    /**
     * Register and enqueue styles.
     *
     * @return $this
     */
    public function loadStyles()
    {
        //  Register
        wp_register_style('main', "$this->tplDir/css/front.css", array(), $this->cacheVersion);

        //  Enqueue
        wp_enqueue_style('main');
        return $this;
    }

    /**
     * Register and enqueue scripts.
     *
     * @return $this
     */
    public function loadScripts()
    {
        //  Register
        wp_register_script('vendors', "$this->tplDir/js/min/vendors.min.js", array("jquery"), $this->cacheVersion, false);
        wp_register_script('scripts', "$this->tplDir/js/min/app.min.js", array("jquery"), $this->cacheVersion, false);


        //  Enqueue
        wp_enqueue_script('vendors');
        wp_enqueue_script('scripts');

        wp_localize_script('scripts', 'ajaxurl', admin_url('admin-ajax.php'));

        return $this;

    }

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::context();';
		$context['menu'] = new Timber\Menu();
		$context['site'] = $this;
		$context['options'] = get_fields('option');
		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This Would return 'foo bar!'.
	 *
	 * @param string $text being 'foo', then returned 'foo bar!'.
	 */
	public function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );

		$twig->addExtension(new \Twig\Extension\DebugExtension());

		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}

}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

new StarterSite();
