<?php
/*
Template Name: HomePage
*/

/**
 * The template for displaying all pages.
 *

 * To generate specific templates for your pages you can use:
 * /mytheme/templates/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;
$context['highlighted'] = \Timber\Timber::get_posts(
	[
		'post_type'		 => 'post',
		'post_per_page'  => 1,
		'post_status'    => 'publish',
		'tax_query'		 => [
			[
				'taxonomy' => 'category',
				'terms'	   => [
					get_field('cat_events', 'option')
				],
				'field'    => 'id'
			]
		]
	]
);
Timber::render( 'homepage.twig', $context );
