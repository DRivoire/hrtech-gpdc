<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-company.twig', 'index.twig' );

$context = Timber::context();
// $context['options'] = get_fields('options');

$timber_post = new Timber\Post();
$context['post'] = $timber_post;

$context['posts'] = new Timber\PostQuery();

$context['sector_terms'] = get_terms( array(
    'taxonomy' => 'sectors',
    'hide_empty' => false,
) );

Timber::render( $templates, $context );
