HRTECH
=========
    
*Site vitrine Wordpress.*

**Versions**  
- Wordpress 5.3.2
- PHP 7.2
- Node v10.18.1


Installation
------------

- git clone git@gitlab.com:DRivoire/hrtech-gpdc.git hrtech

- Modifier le wp-config.php en fonction de votre environnement

- Importer la base de donnée de prod.

- Télécharger les fichiers d'uploads : (en FTP = public/wp-content/uploads wp-content/uploads)

Mise en place BACK
---

1/ S'assurer que les plugins suivant sont installés et activés :
- Advanced custom field PRO ( pour gérer tous les champs personnalisés )  
- Contact Form 7 ( pour gérer vos formulaires )  
- Timber ( pour la compatibilité du thème )  
- Tarteaucitron ( pour ajouter une popin rgpd, à utiliser impérativement si vous souhaitez ajouter un tracker analytics )  
- Yoast SEO ( qui vous servira à optimiser votre référencement )  

2/ Activé le thème "HR Tech" dans Apparence>Thèmes

3/ Créer vos pages d'accueil et d'archive de vos articles dans Pages>Ajouter

4/ Définir ces pages comme étant votre page d'accueil et votre page des article dans Réglages>Lecture

5/ Les champs personnalisés
Dans ACF>Groupes de champs, vous devriez trouvez plusieurs entrées :
- Articles
- Entreprises
- Front page
- Page d'option

Si ce n'est pas le cas, vous pourrez les importer grâce au fichier acf.json que nous vous avons fourni ( Vous pourrez l'ajouter dans ACF > Outils)

Le site est géré avec Twig comme moteur de template, timber est le plugin qui permet son utilisation sur le CMS wordpress.  

Pour ce qui est des champs personnalisés, la plupart est gérée avec ACF, la licence en place est la licence professionnelle de Dimitri Rivoire mais vous pouvez la laisser en place. Il n'y a, pour le moment, pas de limite de sites actifs (mais ce n'est pas garantie que cela reste le cas ...)

Mise en place FRONT
---
- Installer toutes les dépendances nécessaires au développement (css & js) via npm : `npm i` (dans une console)
- Nous utilisons gulp pour compiler le scss (voir `wp-content/themes/hrtech/gulpfile.js & [Gulp](https://gulpjs.com/))   
- Nous utilisons également un Browsersync pour recharger automatiquement notre site durant le développement (voir [BrowserSync](https://browsersync.io/))

Liste des tâches gulp :
- `gulp sass`= compile les fichiers scss en fichiers css
- `gulp scripts-app`= compile/concate/minifie les fichiers javascript du site
- `gulp scripts-vendors`= compile/concate/minifie les fichiers vendors du site (vendors = librairies externes)
- `gulp default` = lance `gulp sass` + `gulp scripts-app` + `scripts-vendors`
- `gulp watch` : lance le Browsersync

Good to know
------------
Comme dit nous utilisons des fiches Scss (que nous compilons en CSS via Gulp)
Le Scss nous permet d'utiliser des variable et des mixins, eg:

utilisez `$colors-primary` au lieu de `#0D0040;`  
utilisez `$font-biotif` au lieu de `'Biotif', sans-serif`  
utilisez `@include FontSize(12px)`  au lieu de `font-size:12px`  
utilisez `@include respond-to(s){}` au lieu de `@media only screen and (min-width: $breakpoint-s) {}`  

Prendre connaissance à minima de `wp-content/themes/hrtech/sass/base` et de `wp-content/themes/hrtech/sass/tools` avant de faire du développement front

Update Wordpress
----------------
ToDO

Migration
---------

ToDO
